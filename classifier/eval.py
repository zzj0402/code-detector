import numpy as np
import os
import tensorflow_hub as hub
import tensorflow as tf
from sklearn.metrics import classification_report
import sys
import csv

csv.field_size_limit(sys.maxsize)


def load_model(path):
    """Loads a model from a .h5 file."""
    sys.path.append(os.path.dirname(path))
    return tf.keras.models.load_model(path, custom_objects={'KerasLayer': hub.KerasLayer})


model = load_model('classifier/model.h5')
model.summary()


def read_csv_file(path):
    data = []
    with open(path, 'r') as f:
        reader = csv.reader(f)
        headers = next(reader)
        for row in reader:
            data.append(row[1])
    return data


code_data = read_csv_file('data/code/snippets/sample_stratified.csv')
descriptions = code_data[3333:3333 + 99]
truths = [1 for _ in range(99)]


def read_txt(path):
    with open(path, 'r') as f:
        data = f.readlines()
    return data


language_data = read_txt('data/natural-question/contexts.txt')
descriptions.extend(language_data[3333:3333 + 99])
truths.extend([0 for _ in range((99))])

prompts = ['This is a test string',
           "model =load_model('classifier/model.h5') model.summary()",
           'Currently, all cultural initiatives undertaken in Nadrupe included the direction of ADRCN (Sport Recreation and Cultural Association of Nadrupe), created by the residents for these and other purposes. The event of greater relevance of the village is the August festival (held on the first Sunday of month, in honor of Our Lady of Grace). Linked to this festival, is also the tradition of the arc, which is the drafting of an arc with around 20m. This arc is decorated with murta Myrtus, lights and flowers and has always religious grounds. The whole arc is rebuilt every year.',
           'The village has a small library',
           'import numpy as np \n import matplotlib.pyplot as plt \n from matplotlib.colors import ListedColormap \n from sklearn.model_selection \n import train_test_split \n from sklearn.preprocessing import StandardScaler \n from sklearn.pipeline import make_pipeline\n from sklearn.datasets import make_moons, make_circles, make_classification \n from sklearn.neural_network import MLPClassifier \n from sklearn.neighbors import KNeighborsClassifier\n from sklearn.svm import SVC\n from sklearn.gaussian_process import GaussianProcessClassifier \nfrom sklearn.gaussian_process.kernels import RBF\n from sklearn.tree import DecisionTreeClassifier\n from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier\n from sklearn.naive_bayes import GaussianNB \n from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis \n from sklearn.inspection import DecisionBoundaryDisplay',
           'The village'
           ]
predictions = model.predict(prompts)
predictions = np.argmax(predictions, axis=1)
print(predictions)
# c_r = (classification_report(truths, predictions))
# print(c_r)
