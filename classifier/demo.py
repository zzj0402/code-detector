import os
import sys

import numpy as np
import tensorflow_hub as hub
import tensorflow as tf


def load_model(path):
    """Loads a model from a .h5 file."""
    sys.path.append(os.path.dirname(path))
    return tf.keras.models.load_model(path, custom_objects={'KerasLayer': hub.KerasLayer})


def detect(prompts):
    model = load_model('classifier/model.h5')
    model.summary()
    predictions = model.predict(prompts)
    predictions = np.argmax(predictions, axis=1)
    print(predictions)
    return predictions
