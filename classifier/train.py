import csv
import sys
import tensorflow as tf
import tensorflow_hub as hub
print("Num GPUs Available: ", len(tf.config.list_physical_devices('GPU')))
tf.device('/device:gpu:0')
module_url = "https://tfhub.dev/google/universal-sentence-encoder-large/5"
model = tf.keras.Sequential()
model.add(tf.keras.layers.Input(shape=[], name='sent1', dtype=tf.string))
model.add(hub.KerasLayer(module_url, trainable=True))
model.add(tf.keras.layers.Dense(2, activation='softmax', name='output'))

model.compile(loss='sparse_categorical_crossentropy',
              optimizer=tf.keras.optimizers.Adam(
                  learning_rate=0.0001,
                  beta_1=0.9,
                  beta_2=0.999,
                  epsilon=1e-07,
                  amsgrad=False,
                  name='Adam'
              ), metrics=['accuracy'])
model.summary()


def read_jsonl(file_path):
    with open(file_path, 'r') as f:
        for line in f:
            yield eval(line)


csv.field_size_limit(sys.maxsize)


def read_csv_file(path):
    data = []
    with open(path, 'r') as f:
        reader = csv.reader(f)
        headers = next(reader)
        for row in reader:
            data.append(row[1])
    return data


code_data = read_csv_file('data/code/snippets/sample_stratified.csv')
descriptions = code_data[:999]


def read_txt(path):
    with open(path, 'r') as f:
        data = f.readlines()
    return data


labels = [1 for _ in range(999)]
language_data = read_txt('data/natural-question/contexts.txt')
descriptions.extend(language_data[:999])
labels.extend([0 for _ in range((999))])
model.fit(descriptions, labels, epochs=9)
model.save('classifier/model.h5')
