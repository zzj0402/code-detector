import sys
import csv

csv.field_size_limit(sys.maxsize)


def read_csv_file(path):
    data = []
    with open(path, 'r') as f:
        reader = csv.reader(f)
        headers = next(reader)
        for row in reader:
            data.append(row[1])
    return data


code_data = read_csv_file('data/code/snippets/sample_stratified.csv')


def read_txt(path):
    with open(path, 'r') as f:
        data = f.readlines()
    return data


language_data = read_txt('data/natural-question/contexts.txt')


def make_jsonl(code_data, language_data, path):
    with open(path, 'w') as f:
        for c in code_data:
            f.write('1 '+c+'径\n')
        for l in language_data:
            f.write('0 '+l+'径\n')


make_jsonl(code_data, language_data, 'data/snippets_contexts.jsonl')
