import sys
import csv

csv.field_size_limit(sys.maxsize)


def read_csv_file(path):
    data = []
    with open(path, 'r') as f:
        reader = csv.reader(f)
        headers = next(reader)
        for row in reader:
            data.append(row[1])
    return data


data = read_csv_file('data/code/snippets/sample_stratified.csv')
